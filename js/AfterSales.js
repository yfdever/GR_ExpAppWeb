/**
 * Created by admin on 2015/12/3.
 */
var BTN_TYPE = {pond:'#',retrunGoods:'app.returnGoodsMission',exchangeGoods:'app.exchangeGoodsMission'};
var ORDER_TYPE = {pond:'#',retrunGoods:11,exchangeGoods:12};
var MISSION_TITLE = {pond:'#',retrunGoods:"退货任务",exchangeGoods:"换货任务"};
//售后首页
app.controller('AfterSalesCtr', ['$scope','$state','$stateParams','appFactory','missionFactory',function($scope, $state,$stateParams,appFactory,missionFactory) {
    appFactory.ready().then(function(){
        $scope.initPage();
    });
    var loadSomeOrders = function(){
        $scope.initPage() ;
    };
    $scope.doRefresh = function(){
        loadSomeOrders();
    }
    var uid = appFactory.getUserInfo().uid;
    var area = appFactory.globalArea();
    var shopArea = appFactory.shopArea();
    $scope.initPage = function(){
        missionFactory.getCountInMission({'areaId':appFactory.getUserInfo().user.area}).then(function(count){
            $scope.returnGoodsCount = count.returnGoodsCount ||0;
            $scope.exchangeGoodsCount = count.exchangeGoodsCount ||0;
            $scope.chiziCount = count.chiziCount||0;
        }).catch(function(err){
        }).finally(function(){
            $scope.$broadcast('scroll.refreshComplete');
        });
    };
    $scope.getURL = function(data){
        $state.go(BTN_TYPE[data],{orderType:ORDER_TYPE[data],title:MISSION_TITLE[data],typeName:data});
    }
}]);
//我的退货任务
app.controller('returnGoodCtr', ['$scope','$state','$stateParams','appFactory','missionFactory',function($scope, $state,$stateParams,appFactory,missionFactory) {
    appFactory.ready().then(function(){
        $scope.missions();
    });
    var uid = appFactory.getUserInfo().uid;
    var area = appFactory.globalArea();
    var shopArea = appFactory.shopArea();
    var orderType = $stateParams.orderType;
    var typeName = $stateParams.typeName;
    $scope.title = $stateParams.title;
    $scope.mymissions=[];
    $scope.arrays=[];
    $scope.loadMore = {more:'加载更多'};
    $scope.page = 1;
    var loadSomeOrders = function(){
        $scope.missions() ;
    };
    $scope.doRefresh = function(){
        $scope.mymissions=[];
        $scope.page = 1;
        loadSomeOrders();
    }

    $scope.missions = function(){
        missionFactory.getCountOfSales(orderType,uid,typeName).then(function(count){
            if(count - $scope.mymissions.length>0){
                $scope.loadMore = '加载更多';
                missionFactory.getSalesOrder(orderType,uid,$scope.page,typeName).then(function(data){
                    if(data){
                        var array = [];
                        $scope.mymissions=$scope.mymissions.concat(data);
                    }
                }).catch(function(){

                })
            }else{
                $scope.loadMore = '~没有了~';
            }
        }).catch(function(err){
        }).finally(function(){
            $scope.$broadcast('scroll.refreshComplete');
        });
    };
    $scope.loadMoreOrder = function(){
        $scope.page+=1;
        $scope.missions();
    }
    //$scope.missions();
}]);
