app.controller('AccountCtrl',['$scope','$ionicActionSheet','$ionicHistory','$timeout','appFactory','$state',
    function($scope,$ionicActionSheet,$ionicHistory,$timeout,appFactory,$state) {
    var Courier = AV.Object.extend('Courier');
    /**
     * 用户信息初始化
     */
    $scope.accountInit=function(){
        $scope.user=appFactory.getUserInfo().user;
        $scope.$broadcast('scroll.refreshComplete');
    }
    $scope.accountInit();

    /**
     * 用户注销
     */
    $scope.loginOut=function(){
        appFactory.loginOut();
        $state.go('app.orders');
        //$ionicHistory.clearHistory();
    }

    $scope.user=appFactory.getUserInfo().user;
    $scope.hasMore1 = true;
    $scope.hasMore2 = true;
    $scope.orders1 = null;
    $scope.orders2 = null;
    $scope.userInfo = null;

    /**
     * 快递员数据汇总
     */
    $scope.loadUserInfo = function(){
        var courierQuery = new AV.Query(Courier);
        courierQuery.get($scope.user.uid).then(function(res){
            $scope.userInfo = res;
        }).catch(function(err){
            console.log(err);
        }).finally(function(){
            $scope.$broadcast('scroll.refreshComplete');
        });
    };

    $scope.doRefresh = function(){
        $scope.loadUserInfo();
    }

    $scope.loadUserInfo();
}]);