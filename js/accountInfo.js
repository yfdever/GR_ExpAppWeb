app.controller('AccountInfoCtrl',['$scope','$ionicActionSheet','$timeout','appFactory','$state',function($scope,$ionicActionSheet,$timeout,appFactory,$state) {
    var storage = window.localStorage;
    var Courier = AV.Object.extend('Courier');
    /**
     * 基础信息初始化
     */
    $scope.useInfoInit=function(){
        //$scope.globalArea=appFactory.globalArea();
        $scope.user=appFactory.getUserInfo().user;
        //$scope.newAreaCN=$scope.user.areaCN;
        //$scope.newAreaName=$scope.user.areaName;
        //$scope.Area=new Array();
        //$scope.AreaId=new Array();
        //$scope.AreaCN=new Array();
        //var Courier = AV.Object.extend('Courier');
        ////获取地址config信息
        //angular.forEach($scope.globalArea,function(data,index,array){
        //    //console.log(data);
        //    $scope.Area.push(data.areaName);
        //    $scope.AreaId.push(data.id);
        //    $scope.AreaCN.push(data.whName);
        //})
        $scope.$broadcast('scroll.refreshComplete');
    }

    $scope.useInfoInit();

    /**
     * 初始化当日派送清单
     */
    $scope.finishInit = function(){
        var uid = appFactory.getUserInfo().uid;
        var Order = AV.Object.extend("Order");
        var now = new Date();

        var year = now.getFullYear();       //年
        var month = now.getMonth() + 1;     //月
        var day = now.getDate();            //日

        if(month<10){
            month = "0"+month;
        }
        if(day<10){
            day = "0"+day;
        }
        var rq = ""+year+"-"+month+"-"+day;

        var ORDER_STATUS = [2000,2500,3000,4000,8000,9000,9999];
        var query = new AV.Query(Order);
        query.containedIn('status',ORDER_STATUS);
        query.equalTo("messionedUser",uid+"");
        //query.greaterThanOrEqualTo("endTime",new Date(rq+' 00:00:01'));
        query.greaterThanOrEqualTo("endTime",new Date(new Date(rq).toDateString() +' 00:00:00'));
        //query.lessThanOrEqualTo("endTime",new Date(rq+' 23:59:59'));
        query.lessThanOrEqualTo("endTime",new Date(new Date(rq).toDateString() +' 23:59:59'));
        query.addDescending("endTime");

        query.find().then(function(res){
            $scope.$apply(function(){
                $scope.ordersArray = res;
                $scope.ordersCount = res.length;
            });
        }).catch(function(err){
            $scope.ordersCount = 0;
        }).finally(function(){
            $scope.$broadcast('scroll.refreshComplete');
        });

    }
    $scope.finishInit();

    /**
     * 分区列表
     * @constructor
     */
    $scope.Sheet = function() {
        var btnArr=[];
        for(var i=0;i<$scope.Area.length;i++){
            btnArr.push({ text:$scope.Area[i] });
        }
        var hideSheet = $ionicActionSheet.show({
            buttons:btnArr,
            titleText: '分区列表',
            cancelText: 'Cancel',
            cancel: function(){
            },
            buttonClicked: function(index) {
                $scope.user.area=$scope.AreaId[index];
                $scope.user.areaName=$scope.Area[index];
                $scope.user.areaCN=$scope.AreaCN[index];
                $scope.newAreaCN=$scope.AreaCN[index];
                $scope.newAreaName=$scope.Area[index];
                hideSheet();
            }
        });
        $timeout(function() {
            hideSheet();
        }, 5000);
    };

    /**
     * 基础信息修改
     */
    $scope.saveInfo = function() {
        query=new AV.Query(Courier);
        query.get($scope.user.uid).then(function(res){
            res.set("phone",$scope.user.phone);
            res.set("email",$scope.user.email);
            res.save().then(function(res){
                $scope.user.phone=res.get('phone');
                $scope.user.email=res.get('email');
                //console.log($scope.user);
                var userJson=JSON.stringify($scope.user);
                storage.setItem("user",userJson);
                $state.go('app.account');
                //window.location.href="#/app/account";
            }).catch(function(err){
                alert("保存失败！")
            });
        })
    }
}]);
