// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic','ionic.service.core','starter.controllers','ngCordova'])
.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    //初始化avos
    AV.initialize('8c4dst8wuiimm9nxj8puwrqrhmkxegkl2ittuy6a2jkmx1pl', '6lqn0keuf89uzoj8px4t47u9e39eimx7jo7ad1qy9pd125nm');
    //测试环境
    //AV.initialize('64yipwcn159l9ehdf3j3x3xz1f1ly9r89x9gf7i2d1282dvz', 'w3xp0oibbzhweamm76ogo6r5dyzt82tczw7lssfvyj6hlarv');
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })
  /**
   *个人中心
   * @author Scor
   */
  .state('app.account', {
    url: '/account',
          cached:false,
    views: {
      'menuContent': {
        templateUrl: 'templates/account.html',
        controller: 'AccountCtrl'
      }
    }
  })

  /**
   * 退换货管理
   */
      .state('app.returnOrders',{
          url:'/returnManager/returnOrders',
          views: {
              'menuContent': {
                  templateUrl: 'templates/returnOrders.html',
                  controller: 'ReturnOrderCtrl'
              }
          }
      })

  /**
   * 设置
   */
      .state('app.setting',{
          url:'/setting',
          views: {
              'menuContent': {
                  templateUrl: 'templates/setting.html',
              }
          }
      })

  /**
   * 设置--分享二维码
   */
      .state('app.QRcode',{
          url:'/setting/QRcode',
          views: {
              'menuContent': {
                  templateUrl: 'templates/qrcode.html'
              }
          }
      })

  /**
   *个人信息修改
   *@author Scor
   */
   .state('app.baseinfo', {
        url: '/account/baseinfo',
          cached:false,
        views: {
          'menuContent': {
            templateUrl: 'templates/account/baseinfo.html',
            controller: 'AccountInfoCtrl'
          }
        }
      })

  /**
   *个人信息修改
   *@author James
   */
      .state('app.finish', {
          url: '/account/finish',
          cached:false,
          views: {
              'menuContent': {
                  templateUrl: 'templates/account/finish.html',
                  controller: 'AccountInfoCtrl'
              }
          }
      })

   .state('app.password', {
          url: '/account/password',
          cached:false,
          views: {
              'menuContent': {
                  templateUrl: 'templates/account/password.html',
                  controller: 'PasswordCtrl'
              }
          }
   })
  /**
   *营销商品
   * @author Scor
   */
      .state('app.goodsType', {
          url: '/markettype',
          views: {
              'menuContent': {
                  templateUrl: 'templates/markettype.html',
                  controller: 'GoodsType'
              }
          }
      })
   .state('app.goodslist', {
        url: '/marketgoods/:search/:type',
        cached:false,
        views: {
           'menuContent': {
            templateUrl: 'templates/marketgoods.html',
            controller: 'GoodsListCtrl'
          }
        }
   })
   .state('app.marketOrders', {
        url: '/marketOrders',
        views: {
           'menuContent': {
              templateUrl: 'templates/marketOrders.html',
              controller: 'MarketOrderCtrl'
           }
          }
   })
  .state('app.receiveOrder', {
    url: '/receiveOrder',
    views: {
      'menuContent': {
        templateUrl: 'templates/receiveOrder.html',
        controller:'receiveOrderCtrl'
      }
    }
  })
      //手动领单
  .state('app.handlerReceiveOrder', {
       url: '/handlerReceiveOrder',
       views: {
          'menuContent': {
              templateUrl: 'templates/handlerReceiveOrder.html',
              controller:'handlerReceiveOrderCtrl'
          }
       }
  })
  .state('app.browse', {
      url: '/browse',
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html'
        }
      }
    })
    .state('app.orders', {
      url: '/orders',
      views: {
        'menuContent': {
          templateUrl: 'templates/orders.html',
          controller: 'OrderCtrl'
        }
      }
    })

  .state('app.deliverypay', {
      url: '/account/deliverypay',
      views: {
          'menuContent': {
              templateUrl: 'templates/account/deliverypay.html',
              controller: 'AccountCtrl'
          }
      }
  })

  .state('app.marketpay', {
      url: '/account/marketpay',
      views: {
          'menuContent': {
              templateUrl: 'templates/account/marketpay.html',
              controller: 'AccountCtrl'
          }
      }
  })
  //二娃strat
      .state('app.baoyue',{
          url: '/baoyue/baoyuelist',
          views: {
              'menuContent': {
                  templateUrl: 'templates/Baoyue/baoyuelist.html',
                  controller: 'conBaoyueCtrl'
              }
          }
      })
      .state('app.baoyuedetails',{
          url: '/baoyue/baoyuedetails/:orderdetails',
          views: {
              'menuContent': {
                  templateUrl: 'templates/Baoyue/baoyuedetails.html',
                  controller: 'conBaoyueDetailsCtrl'
              }
          }
      })

  //end

  .state('app.single', {
    url: '/playlists/:playlistId',
    views: {
      'menuContent': {
        templateUrl: 'templates/playlist.html',
        controller: 'PlaylistCtrl'
      }
    }
  })
      .state('app.missions', {
          url: '/missions',
          views: {
              cashed:false,
              'menuContent': {
                  templateUrl: 'templates/mission/missions.html',
                  controller: 'MissionsCtrl'
              }
          }
      })
      .state('app.mymission', {
          url: '/mymission/:status/:type/:orderid/:title',
          views: {
              cashed:false,
              'menuContent': {
                  templateUrl: 'templates/mission/mymission.html',
                  controller: 'MyMissionCtrl'
              }
          }
      })
      .state('app.missionDetail', {
          url: '/missionDetail/:orderid',
          cashed:false,
          views: {
              'menuContent': {
                  templateUrl: 'templates/mission/missionDetail.html',
                  controller: 'MissionDetailCtrl'
              }
          }
      })
      .state('app.completMission', {
          url: '/completMission/:orderid/:payway/:finalAmount',
          cashed:false,
          views: {
              'menuContent': {
                  templateUrl: 'templates/mission/completMission.html',
                  controller: 'CompletMissionCtrl'
              }
          }
      })

      .state('app.afterSales',{
          url:'/afterSales',
          cashed:false,
          views:{
              'menuContent':{
                  templateUrl:'templates/afterSales/afterSales.html',
                  controller:'AfterSalesCtr'
              }
          }
      })
      .state('app.returnGoodsMission',{
          url:'/returnGoodsMission/:orderType/:title/:typeName',
          cashed:false,
          views:{
              'menuContent':{
                  templateUrl:'templates/afterSales/missionDetail.html',
                  controller:'returnGoodCtr'
              }
          }
      })
      .state('app.exchangeGoodsMission',{
          url:'/exchangeGoodsMission/:orderType/:title/:typeName',
          cashed:false,
          views:{
              'menuContent':{
                  templateUrl:'templates/afterSales/missionDetail.html',
                  controller:'returnGoodCtr'
              }
          }
      })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/orders');
});

var service = angular.module('starter.service', []);

var app = angular.module('starter.controllers', ['starter.service','starter.filter']);

