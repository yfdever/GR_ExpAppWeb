app.controller('conBaoyueCtrl',function($scope,$state,appFactory){


    $scope.getlist=function(){
        var workno = appFactory.getUserInfo().user.jobnumber;
        //var workno = "GR0192";
        //console.log(appFactory.getUserInfo());
        var statuslist = new Array(2000,2500,3000,4000,4500,5000,6000,7000,8000);
        var Order = AV.Object.extend('Order');
        var queryOrder = new AV.Query(Order);
        queryOrder.equalTo("sellerNo",workno);
        queryOrder.equalTo("type",7);
        //queryOrder.equalTo("orderfinish","1");// 1表示为派送，2表示包月套餐全部派送完成
        queryOrder.find().then(function(orderlist){
            if(orderlist){
                //先判断出由几个包月套餐
                var tcnamelist = new Array();
                for(var j=0;j<orderlist.length;j++){
                    var ordersno=orderlist[j].get("orderid");
                    var orderno=ordersno.substring(0,ordersno.indexOf('-'));
                    if(tcnamelist.indexOf(orderno)==-1){
                        tcnamelist.push(orderno);
                    }
                }
                var porderlist = new Array();
                for(var k=0;k<tcnamelist.length;k++){
                    var dadorder = new Object();
                    var weipai=0;//尚未派送的子订单数
                    dadorder.ziorder = new Array();
                    for(var a=0;a<orderlist.length;a++){
                        var sorderno = orderlist[a].get("orderid");
                        var sordno=sorderno.substring(0,sorderno.indexOf('-'));
                        if(tcnamelist[k]==sordno){//判断订单编号是否在父订单中
                            var sonorder = new Object();
                            sonorder.orderid = orderlist[a].get("orderid");
                            sonorder.name = orderlist[a].get("address").name;
                            sonorder.phone = orderlist[a].get("address").phone;
                            sonorder.address = orderlist[a].get("address").address;

                            var goods = orderlist[a].get("goods");
                            sonorder.proname = goods[0].pname;
                            sonorder.sname = goods[0].sname;

                            sonorder.tcname = orderlist[a].get("bytcname");
                            sonorder.bookdate = orderlist[a].get("bookdate");
                            sonorder.bookdate = new Date(parseInt(sonorder.bookdate) * 1000).toLocaleString();
                            sonorder.status = orderlist[a].get("status");
                            var statuso = orderlist[a].get("status");
                            switch (statuso){
                                case 1000:
                                    sonorder.statuschn='已创建、未付款';
                                    break;
                                case 2000:
                                    sonorder.statuschn='已付款、未审核';
                                    break;
                                case 2500:
                                    sonorder.statuschn='已审核、未出单';
                                    break;
                                case 3000:
                                    sonorder.statuschn='已出单、未打包';
                                    break;
                                case 4000:
                                    sonorder.statuschn='已打包、未中转';
                                    break;
                                case 4500:
                                    sonorder.statuschn='已中转、未抵达';
                                    break;
                                case 5000:
                                    sonorder.statuschn='已抵达、未分配';
                                    break;
                                case 6000:
                                    sonorder.statuschn='已分配、未领件';
                                    break;
                                case 7000:
                                    sonorder.statuschn='已领件、未派送';
                                    break;
                                case 8000:
                                    sonorder.statuschn='已派送、未签收';
                                    break;
                                case 9000:
                                    sonorder.statuschn='已签收、未评价';
                                    break;
                                case 9001:
                                    sonorder.statuschn='退货待受理';
                                    break;
                                case 9002:
                                    sonorder.statuschn='退货受理中';
                                    break;
                                case 9003:
                                    sonorder.statuschn='退货已受理';
                                    break;
                                case 9999:
                                    sonorder.statuschn='已评价';
                                    break;
                                case -1:
                                    sonorder.statuschn='订单用户取消';
                                    break;
                                case -2:
                                    sonorder.statuschn='订单平台取消';
                                    break;
                            }
                            //console.log(statuso);
                            if(statuslist.indexOf(statuso)!=-1){ //需要优化，添加一个订单状态数组，判断订单是否派送
                                //alert('ff');
                                weipai = weipai+1;
                            }
                            dadorder.ziorder.push(sonorder);
                            dadorder.nameo = orderlist[a].get("address").name;
                            dadorder.phone = orderlist[a].get("address").phone;
                            dadorder.address = orderlist[a].get("address").address;
                            dadorder.tcname = orderlist[a].get("bytcname");
                            dadorder.bookdate = orderlist[a].get("bookdate");
                            dadorder.bookdate = new Date(parseInt(dadorder.bookdate) * 1000).toLocaleString();
                        }
                        dadorder.peisong=weipai;
                    }
                    if(dadorder.peisong>0){
                        porderlist.push(dadorder);
                    }
                }
                $scope.baorderlists=porderlist;
            }
        }).catch(function(err){
            console.log(err);
            $scope.$broadcast('scroll.refreshComplete');
        }).finally(function(){
            //alert('refreshComplete');
            $scope.$broadcast('scroll.refreshComplete');
        });
    };

    $scope.getlist();

    $scope.doRefresh = function(){
        $scope.getlist();
    };

    $scope.showDetail=function(args){
        //alert(args);
        //window.location.href='#/app/baoyue/baoyuedetails';
        //console.log(args);
        $state.go('app.baoyuedetails',{orderdetails:JSON.stringify(args)}) ;
    };
});

app.controller('conBaoyueDetailsCtrl',function($scope,$state,$stateParams){
    var order=$stateParams.orderdetails;
    //console.log(order);
    $scope.afaorder=JSON.parse(order);

    $scope.getdate=function(args){
        //alert(args.yydate +'-'+ args.yyhour);
        var yydate = new Date(args.yydate);
        var yyhour = args.yyhour;
        yydate.setHours(yyhour);
        //console.log(yydate);
        var today = new Date();
        if(yydate.toString()=='Invalid Date'){
            alert('请选择日期')
        }else if(yydate<today){
            alert('预约时间不能穿越哦')
        }else if(args.status ==2500 || args.status ==2000){
            var nian=yydate.getFullYear();
            var yue = yydate.getMonth()+1;
            var ri = yydate.getDate();
            var hour = args.yyhour;
            var newyysj=nian+'/'+yue+'/'+ri+' '+hour+':00:00';
            //console.log(newyysj);
            var newyydate = new Date(newyysj);
            var updatebookdate = Date.parse(newyydate)/1000;
            //alert(args.orderid);

            //console.log(Date.parse(newyydate)/1000);
            //更新预约时间
            var Orderc = AV.Object.extend('Order');
            var queryOrderc = new AV.Query(Orderc);
            queryOrderc.equalTo('orderid',args.orderid);
            queryOrderc.first().then(function(res){
                res.set('bookdate',updatebookdate.toString());
                if(res.get("syncStatus")!=1){
                    res.set('syncStatus',2);
                }
                res.save().then(function(res){
                    args.bookdatenew='k';
                    alert('预约时间更新成功');
                }).catch(function(err){
                    console.log(err);
                })
            })
        }else {
            alert('该订单状态，已不可修改预约时间')
        }
    };
});