var filter = angular.module('starter.filter', []);
var storage = window.localStorage;
filter.filter('area',function(){
    return function(aid){
        var area = storage.getItem('globalArea');
        area = JSON.parse(area);
        return area[aid].areaName || '未识别';
    }
}).filter('shop',function(){
    return function(sid){
        var shop = storage.getItem('shopArea');
        shop = JSON.parse(shop);
        return shop[sid].whName || '未识别';
    }
}).filter('dateFormat',function(){
    return function(item){
        //alert(item)
        return new Date(item).toLocaleString();
    }
});

filter.filter('deltaDate',function(){
    return function(item){
        if(item == 'undefined' || !item){
            return ' -- ';
        }
        if((''+item).length > 10){
            return parseInt(((new Date().getTime() - new Date(item).getTime())/1000/60));
        }else{
            return parseInt(((new Date().getTime()/1000 - new Date(item).getTime())/60));
        }
    }
});

var orderStaeArray = {
    1000:'已创建，未付款',
    2000:'已付款，未审核',
    2500:'已审核，未出单',
    3000:'已出单，未打包',
    4000:'已打包，未中转',
    4500:'已中转，未抵达',
    5000:'已抵达，未分配',
    6000:'已分配，未领件',
    7000:'已领件，未派送',
    8000:'已派送，未签收',
    9000:'已签收，未评价',
    9001:'退货待受理',
    9002:'退货受理中',
    9003:'退货已受理',
    999:'已评价',
    '-1':'用户取消',
    '-2':'平台取消'
};
filter.filter('orderState',function(){
    return function(code){
        if(code<0){
            code = code+'';
        }
        return orderStaeArray[code];
    }
});
var orderTpe = {
    9:'商家外卖',
    11:'退货',
    12:'换货'
};
filter.filter('orderTpe',function(){
    return function(code){
        if(code<0){
            code = code+'';
        }
        return orderTpe[code];
    }
});
var orderTypeCode = {
    0:"普",
    1:'团',
    2:'秒',
    3:'普',
    4:'活动',
    5:'预',
    6:'外',
    7:'包月',
    8:'营销',
    9:'三方',
    11:'退货',
    12:'换货'
};
filter.filter('orderTypeName',function(){
    return function(code){
        return orderTypeCode[code];
    }
});