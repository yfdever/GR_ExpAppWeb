app.controller('PasswordCtrl',['$scope','appFactory','$state','$timeout','$ionicModal',function($scope,appFactory,$state,$timeout,$ionicModal) {
    var Courier = AV.Object.extend('Courier');
    ///**
    // * 登出
    // * *@author Scor 2015-10-16
    // */
    //$scope.loginOut=function(){
    //    storage.removeItem("user");
    //    storage.removeItem("globalArea");
    //    storage.removeItem("uid");
    //    //window.location.reload();
    //    //$state.reload();
    //}

    $scope.pwd=appFactory.getUserInfo().user;
    $scope.pwd.pwdfocus=true;
    $scope.pwd.repwdfocus=true;
    $scope.pwd.oldpwdfocus=true;

    $scope.lostFocus=function(arg){
        switch (arg){
            case 0:
                $scope.pwd.oldpwdfocus=false;
                break;
            case 1:
                $scope.pwd.pwdfocus=false;
                break;
            case 2:
                $scope.pwd.repwdfocus=false;
                break;
        }

        //$scope.pwd.pwdformate=false;
    }

    /**
     * 修改密码
     * @author Scor 2015-10-16
     */
    $scope.modifyPwd=function(){
            query=new AV.Query(Courier);
            query.equalTo("name",$scope.pwd.name);
            query.equalTo("pwd",md5($scope.pwd.oldpwd));
            query.first().then(function(res){
                if(res){
                    res.set('pwd',md5($scope.pwd.newpwd));
                    res.set('status',"2");
                    res.save().then(function(){
                        alert("密码修改成功");
                        $scope.pwd.oldpwd="";
                        $scope.pwd.newpwd="";
                        $scope.pwd.renewpwd="";
                        $state.go('app.account');
                        //window.location.href="#/app/account";
                    }).catch(function(){
                        alert("密码修改失败");
                    });

                }else{
                    alert("原密码不正确");
                }
            })

    }

    $scope.pwdSame=function(){
        if($scope.pwd.newpwd==$scope.pwd.renewpwd){
             $scope.pwd.pwdSame=true;
        }else{
            $scope.pwd.pwdSame=false;
        }
    }

}]);

