app.controller('ReturnOrderCtrl', ['$scope','orderFactory','appFactory','$ionicLoading',function($scope,orderFactory,appFactory,$ionicLoading) {
    $scope.returnOrders = [];
    $scope.canRush = 1 ;
    var loadSomeOrders = function(){
        orderFactory.getReturnOrderList({'areaId':appFactory.getUserInfo().user.area})
            .then(function(list){
                if(list.length<orderFactory.PAGE_SIZE){
                    $scope.hasMore = false;
                }
                //对list进行排序
                //优先显示当前分区
                list.sort(function(x,y){
                    //if(x.get('areaId'))
                    var aid = y.get('areaId');
                    return (appFactory.getUserInfo().user.area.indexOf(aid)>-1)?1:-1;
                });
                $scope.returnOrders = list;
            }).finally(function(){
                $scope.$broadcast('scroll.refreshComplete');
            });
    };

    $scope.doRefresh = function(){
        loadSomeOrders();
    }


    $scope.rush = function(event,o,index){
        //更新订单状态，并且移除调该订单

        event = event ? event : window.event;
        var obj = event.srcElement ? event.srcElement : event.target;
        obj.disabled=true;

        $scope.canRush = 0;
        console.log(o);
        if(o.get('messioned')===true){
            return;
        };
        //return;
        orderFactory.rushReturnMession(o).then(function(res){
            o.set('messioned',true);
            alert('抢单成功!快快出发把~');
        }).catch(function(err){
            alert(err.error);
            $scope.canRush = 1;
        });
    }

}]);
